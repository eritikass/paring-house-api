# paringHouseApi.ParkingApi

All URIs are relative to *http://localhost:3857/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**parkingClientIdGet**](ParkingApi.md#parkingClientIdGet) | **GET** /parking/{clientId} | list parkings for client
[**parkingPost**](ParkingApi.md#parkingPost) | **POST** /parking | enter parking event


<a name="parkingClientIdGet"></a>
# **parkingClientIdGet**
> [ParkingInvoice] parkingClientIdGet(clientId)

list parkings for client

### Example
```javascript
var paringHouseApi = require('@eritikass/paring-house-api');

var apiInstance = new paringHouseApi.ParkingApi();

var clientId = "clientId_example"; // String | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.parkingClientIdGet(clientId, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  | 

### Return type

[**[ParkingInvoice]**](ParkingInvoice.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="parkingPost"></a>
# **parkingPost**
> ParkingInvoice parkingPost(parking)

enter parking event

### Example
```javascript
var paringHouseApi = require('@eritikass/paring-house-api');

var apiInstance = new paringHouseApi.ParkingApi();

var parking = new paringHouseApi.Parking(); // Parking | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.parkingPost(parking, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parking** | [**Parking**](Parking.md)|  | 

### Return type

[**ParkingInvoice**](ParkingInvoice.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

