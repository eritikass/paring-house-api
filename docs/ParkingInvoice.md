# paringHouseApi.ParkingInvoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parkingId** | **String** |  | 
**cost** | **Number** |  | [optional] 


