# paringHouseApi.ClientApi

All URIs are relative to *http://localhost:3857/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**clientsGet**](ClientApi.md#clientsGet) | **GET** /clients | get clients list
[**exportClientIdYearMonthGet**](ClientApi.md#exportClientIdYearMonthGet) | **GET** /export/{clientId}/{year}/{month} | get client invoice for given month


<a name="clientsGet"></a>
# **clientsGet**
> [Client] clientsGet()

get clients list

### Example
```javascript
var paringHouseApi = require('@eritikass/paring-house-api');

var apiInstance = new paringHouseApi.ClientApi();

var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.clientsGet(callback);
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**[Client]**](Client.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="exportClientIdYearMonthGet"></a>
# **exportClientIdYearMonthGet**
> File exportClientIdYearMonthGet(clientId, year, month)

get client invoice for given month

### Example
```javascript
var paringHouseApi = require('@eritikass/paring-house-api');

var apiInstance = new paringHouseApi.ClientApi();

var clientId = "clientId_example"; // String | 

var year = 56; // Number | 

var month = 56; // Number | 


var callback = function(error, data, response) {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
};
apiInstance.exportClientIdYearMonthGet(clientId, year, month, callback);
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **String**|  | 
 **year** | **Number**|  | 
 **month** | **Number**|  | 

### Return type

**File**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

