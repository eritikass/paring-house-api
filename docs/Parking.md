# paringHouseApi.Parking

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | [**Client**](Client.md) |  | 
**startTime** | **String** |  | 
**endTime** | **String** |  | 
**parkingHouse** | **String** |  | [optional] [default to &#39;default&#39;]


