# paringHouseApi.Client

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientId** | **String** |  | 
**type** | **String** |  | [optional] 


<a name="TypeEnum"></a>
## Enum: TypeEnum


* `premium` (value: `"premium"`)

* `regular` (value: `"regular"`)




